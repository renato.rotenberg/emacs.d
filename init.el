;;; package --- Summary

;;; Code:

;;; Commentary:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(c-basic-offset 4)
 '(c-default-style "bsd")
 '(menu-bar-mode nil)
 '(scheme-program-name "guile")
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Initialize package and add melpa archives

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

;; Ensure use-package is avalible

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)

;; == monokai ==
(use-package monokai-theme
  :ensure t 
  :init
  (load-theme 'monokai t))

;; == smex ==
(use-package smex
  :ensure t
  :bind (("M-x" . smex)
	 ("M-X" . smex-major-mode-commands)
	 ;; This is the old M-x.
	 ("C-c C-c M-x" . execute-extended-command)))

(use-package nyan-mode
  :ensure t
  :init
  (nyan-mode)
  (setq nyan-wavy-trail t))

;; == magit ==
(use-package magit
  :ensure t
  :defer  t)

;; == projectile ==
(use-package projectile
  :ensure t
  :defer  t)

;; == irony ==
(use-package irony
  :ensure t
  :defer  t
  :init
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)
  :config
  ;; replace the `completion-at-point' and `complete-symbol' bindings in
  ;; irony-mode's buffers by irony-mode's function
  (defun my-irony-mode-hook ()
    (define-key irony-mode-map [remap completion-at-point]
      'irony-completion-at-point-async)
    (define-key irony-mode-map [remap complete-symbol]
      'irony-completion-at-point-async))
  (add-hook 'irony-mode-hook 'my-irony-mode-hook)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

;; == company-mode ==
(use-package company
  :ensure t 
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  :config
  ;; (use-package company-irony :ensure t :defer t)
  ;; (use-package company-c-headers :ensure t :defer t)
  (setq company-idle-delay              0
	company-minimum-prefix-length   2
	company-show-numbers            t
	company-tooltip-limit           20)
  ;; Add yasnippet support for all company backends
  ;; https://github.com/syl20bnr/spacemacs/pull/179
  (defvar company-mode/enable-yas t
    "Enable yasnippet for all backends.")
  (defun company-mode/backend-with-yas (backend)
    (if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
	backend
      (append (if (consp backend) backend (list backend))
	      '(:with company-yasnippet))))
  (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))
  :bind ("C-;" . company-complete-common))

;; == yasnippet ==
(use-package yasnippet
  :ensure t 
  :init
  (use-package clojure-snippets :ensure t :defer t)
  (yas-global-mode 1))

;; == flycheck ==
(use-package flycheck
  :ensure t 
  :init
  (global-flycheck-mode)
  (use-package flycheck-pos-tip :ensure t :defer t)
  ;; (use-package flycheck-clojure :ensure t :defer t)
  (use-package flycheck-irony :ensure t :defer t)
  :config
  (with-eval-after-load 'flycheck (flycheck-pos-tip-mode))
  ;; (eval-after-load 'flycheck '(flycheck-clojure-setup))
  (add-hook 'flycheck-mode-hook #'flycheck-irony-setup)
  (add-hook 'c++-mode-hook (lambda () (setq flycheck-gcc-language-standard "c++11"))))

;; == agressive-indent ==
(use-package aggressive-indent
  :ensure t 
  :init
  (global-aggressive-indent-mode 1)
  (add-to-list 'aggressive-indent-excluded-modes 'html-mode))

;; == cmake-ide ==
(use-package cmake-ide
  :ensure t
  :init
  (cmake-ide-setup))

;; == rainbow-delimiters ==
(use-package rainbow-delimiters
  :ensure t
  :defer  t
  :init
  (add-hook 'emacs-lisp-mode-hook       #'rainbow-delimiters-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'rainbow-delimiters-mode)
  (add-hook 'ielm-mode-hook             #'rainbow-delimiters-mode)
  (add-hook 'lisp-mode-hook             #'rainbow-delimiters-mode)
  (add-hook 'lisp-interaction-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'scheme-mode-hook           #'rainbow-delimiters-mode)
  (add-hook 'clojure-mode-hook          #'rainbow-delimiters-mode)
  (add-hook 'cider-mode-hook            #'rainbow-delimiters-mode))

;; == paredit ==
(use-package paredit
  :ensure t
  :defer  t
  :init
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
  (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
  (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
  (add-hook 'scheme-mode-hook           #'enable-paredit-mode)
  (add-hook 'clojure-mode-hook          #'enable-paredit-mode)
  (add-hook 'cider-mode-hook            #'enable-paredit-mode))

;; == geiser ==
(use-package geiser
  :ensure t
  :defer  t)

;; == clojure-mode ==
(use-package clojure-mode
  :ensure t
  :defer  t)

;; == cider ==
(use-package cider
  :ensure t
  :defer  t)
